import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Link} from 'react-router';

export const fields = ['title', 'categories'];


const validate = values => {
  const errors = {};
  if (!values.title) {
    errors.title = 'Required';
  } else if (values.title.length > 25) {
    errors.title = 'Must be 25 characters or less';
  }
  if (!values.categories) {
    errors.categories = 'Required';
  } else if (values.categories.length > 140) {
    errors.categories = 'Must be 140 characters or less';
  }
  return errors;
};

class WizardFormFirstPage extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired
  };


  render() {
    const {fields: {title, categories}, handleSubmit} = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <div>
          <h3>Create a new post</h3>
          <label>Title</label>
          <div className={`form-group ${title.touched && title.invalid ? 'has-danger' : ''}`}>
            <input type="text" className="form-control" placeholder="Title" {...title}/>
            <div className="text-help"> {title.touched ? title.error : ''} </div>
          </div>
        </div>

        <div>
          <label>Categories</label>
          <div className={`form-group ${categories.touched && categories.invalid ? 'has-danger' : ''}`}>
            <input type="text" className="form-control" placeholder="Categories" {...categories}/>
            <div className="text-help"> {categories.touched ? categories.error : ''} </div>
          </div>
        </div>
        
        <div>
          <Link to="/" className="btn btn-danger">Cancel</Link>
          <button type="submit" className="btn btn-primary">
            Next <i/>
          </button>
        </div>
      </form>
    );
  }
}

export default reduxForm(
  {
  form: 'wizard',              // <------ same form name
  fields,                      // <------ only fields on this page
  destroyOnUnmount: false,     // <------ preserve form data
  validate                     // <------ only validates the fields on this page
})(WizardFormFirstPage);
