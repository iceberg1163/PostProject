import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {createPost} from '../actions/index';
//export const fields = ['title', 'categories', 'content'];
// ^^ All fields on last form

const validate = values => {
  const errors = {};
  if (!values.content) {
    errors.content = 'Required';
  }
  return errors;
};

class WizardFormSecondPage extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  static propTypes = {
    fields: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    previousPage: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired
  };
  onSubmit(propsFromForm){
    this.props.createPost(propsFromForm).then(() =>{
        this.context.router.push('/');
      });
  }

  render() {

    const {fields: {content},
      handleSubmit,
      previousPage,
      submitting
      } = this.props;

    return (<form onSubmit={handleSubmit(this.onSubmit.bind(this))}>

        <div>
        <h3>Create a new post</h3>
          <label>Content</label>
          <div className={`form-group ${content.touched && content.invalid ? 'has-danger' : ''}`}>
            <textarea className="form-control" {...content} />
              <div className="text-help"> {content.touched ? content.error : ''} </div>
          </div>
        </div>
        <div>
          <button type="button" className="btn btn-danger" disabled={submitting} onClick={previousPage}>
            <i/> Previous
          </button>
          <button type="submit" className="btn btn-primary" >
            Finish
          </button>
        </div>
      </form>
    );
  }
}

export default reduxForm({
  form: 'wizard',                               // <------ same form name
  fields: ['title', 'categories', 'content'],  // <------ all fields on last wizard page
  destroyOnUnmount: false,                      // <------ preserve form data
  validate                                     // <------ only validates the fields on this page
}, null, {createPost})(WizardFormSecondPage);
