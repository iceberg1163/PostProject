import React,{Component, PropTypes} from 'react';
import {Link} from 'react-router';

var errorCode;
class Error extends Component{
  static contextTypes = {
    router: PropTypes.object
  };

render(){
  if(this.props.error)
  {
    errorCode = "Error 404 (Not Found)";
  }
return(
  <div>
    <div className="text-xs-right">
      <Link to="/" className="btn btn-primary">
        Home
      </Link>
    </div>
    <h3>This is an error</h3>
      <p>There may have been no response from the API.</p>
      <p>{errorCode}</p>
  </div>

);
}
}

export default Error;
