import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {viewPost, deletePost} from '../actions/index';
import {Link} from 'react-router';
import Error from './error';

var handleError = false;
var errorCode;

class SelectedPost extends Component{
  static contextTypes = {
    router: PropTypes.object
  };

  componentWillMount(){
    this.props.viewPost(this.props.params.id);
  }

  onDeleteClick(){
    this.props.deletePost(this.props.params.id)
      .then(()=> {this.context.router.push('/');});
  }

  render(){
      console.log(this.props);

    if(isEmpty(this.props.post)){

      //If no Error from the API request
      if(!this.props.error){
            return(
              <div>
              <div className="text-xs-right">
                <Link to="/" className="btn btn-primary">
                  Home
                </Link>
              </div>
                Looking for your post!
              </div>
                  );
                }
      //If there is an Error from the API request
      if(this.props.error){
        return (<div>
                <Error error ={this.props.error}/>
               </div>);
            }
    }

    else {
      return(
      <div>
        <div className="text-xs-right">
          <Link to="/" className="btn btn-primary">
            Home
          </Link>
          <button className="btn btn-danger pull-xs-right"
            onClick={this.onDeleteClick.bind(this)}>
            Delete Post
          </button>
        </div>

      <h3>View Post</h3>

          <h4>{this.props.post.title}</h4>
            <p className="content">{this.props.post.content}</p>

            <Link className="btn btn-edit" to={`/edit/
              ${this.props.post.id}/
              ${this.props.post.title}/
              ${this.props.post.content}/
              ${this.props.post.categories}`}>
              Edit
            </Link>
          <h6><b>Categories</b></h6>
            <p className="categories">{this.props.post.categories}</p>
      </div>
            );
          }
  }
}

function isEmpty(object) {
   for(var key in object) {
      if(object.hasOwnProperty(key)){
     return false;
   }
   }
   return true;
}

function mapStateToProps(state){
  return {post: state.posts.selectedPost, error: state.posts.error};
}

export default connect(mapStateToProps, {viewPost, deletePost})(SelectedPost);
