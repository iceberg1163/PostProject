import React,{Component} from 'react';
import {connect} from 'react-redux';
import {fetchPosts} from '../actions/index';
import {Link} from 'react-router';
import Error from './error';

var handleError = false;
var errorCode;


class PostsIndex extends Component {
componentWillMount(){
  this.props.fetchPosts();
}


renderPosts(){
  return this.props.post.map((post) => {
    return (
      <li className="list-group.item" key={post.id}>
        <Link className="post-links" to={`/posts/selected/${post.id}`}>
          <span className="pull-xs-right">{post.categories}</span>
          <strong>{post.title}</strong>
        </Link>
      </li>
    );
  });
}

  render() {

    //If the API returns with no posts
    if(isEmpty(this.props.post)){

      //If no Error from the API request
      if(!this.props.error){
        return <div>
              <div className="text-xs-right">
                <Link to="/posts/new" className="btn btn-primary">
                  Add a Post
                </Link>
              </div>
                There are no posts to display. Please create one.
              </div>;
            }
      //If there is an Error from the API request
      if(this.props.error){
        return <div>
                <Error error ={this.props.error}/>
              </div>;
            }
      }

      else{
        console.log(this.props);
        return(
          <div>
          <div className="text-xs-right">
            <Link to="/posts/new" className="btn btn-primary">
              Add a Post
            </Link>
            <Link to="/Wizard" className="btn btn-primary">
              Wizard
            </Link>
          </div>
            <h3>Posts</h3>
              <ul className="list-group">
                {this.renderPosts()}
              </ul>
          </div>
        );
        }
  }
}


function isEmpty(object) {
   for(var key in object) {
      if(object.hasOwnProperty(key)){
     return false;
   }
   }
   return true;
}

function mapStateToProps(state){
  return {post: state.posts.all, error: state.posts.error};
  }

export default connect(mapStateToProps, {fetchPosts})(PostsIndex);
