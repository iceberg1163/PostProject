import {FETCH_POSTS, VIEW_POST} from '../actions/index';

const INITIAL_STATE = {all: [], post: null};

export default function (state = INITIAL_STATE, action){
  switch (action.type) {
    case FETCH_POSTS:
      return{...state, all: action.payload.data, error: action.error};

    case VIEW_POST:
    //ternary operator:
    //if an error action.error is null otherwise selected is payload.data
    var selected = action.error ? null : action.payload.data;
      return{...state, selectedPost: selected, error: action.error};

  default:
    return state;
  }
}
