import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from "./components/app";
import PostsIndex from './components/posts_index';
import PostsNew from './components/posts_new';
import SelectedPost from './components/post_selected';
import WizardForm from './components/WizardForm';
import EditPost from './components/editpost';

export default (
<Route path="/" component ={App}>
  <IndexRoute component ={PostsIndex} />
  <Route  path="posts/new" component ={PostsNew} />
  <Route path ="posts/selected/:id" component = {SelectedPost} />
  <Route path ="/wizard" component={WizardForm} />
  <route path="/edit/:id/:title/:content/:categories" component={EditPost} />
</Route>
);
